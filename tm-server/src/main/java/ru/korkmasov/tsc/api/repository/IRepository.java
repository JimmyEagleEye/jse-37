package ru.korkmasov.tsc.api.repository;

import ru.korkmasov.tsc.model.AbstractEntity;

import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface IRepository<E extends AbstractEntity> {

    void add(@Nullable E entity);

    @Nullable
    E findById(@NotNull String id) throws SQLException;

    void addAll(final Collection<E> collection);

    void clear() throws SQLException;

    void remove(@Nullable E entity);

    void removeById(@NotNull String id) throws SQLException;

    int size(@NotNull String testUserId);

    @NotNull
    List<E> findAll();

}
