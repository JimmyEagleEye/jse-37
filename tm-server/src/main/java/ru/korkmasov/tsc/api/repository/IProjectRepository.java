package ru.korkmasov.tsc.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import lombok.SneakyThrows;
import ru.korkmasov.tsc.model.Project;

public interface IProjectRepository extends IOwnerRepository<Project> {

    @SneakyThrows
    @Nullable
    Project add(@Nullable Project entity);

    @Nullable
    @SneakyThrows
    Project update(@Nullable Project entity);

    boolean existsByName(@NotNull String userId, @NotNull String name);

    @Nullable
    Project findOneByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    Project findOneByName(@NotNull String userId, @NotNull String name);

    @Nullable
    Project removeOneByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    void  removeOneByName(@NotNull String userId, @NotNull String name);

    //@Nullable
    //String getIdByName(@NotNull String userId, @NotNull String name);

}
