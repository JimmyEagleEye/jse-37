package ru.korkmasov.tsc.exception.system;

import org.jetbrains.annotations.NotNull;
import ru.korkmasov.tsc.exception.AbstractException;

public class DatabaseInitException extends AbstractException {

    @NotNull
    public DatabaseInitException() {
        super("Error. Database initialization failed.");
    }

}
