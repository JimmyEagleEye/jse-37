package ru.korkmasov.tsc.exception.entity;

import ru.korkmasov.tsc.exception.AbstractException;

public class StatusNotFoundException extends AbstractException {

    public StatusNotFoundException() {
        super("Error. Task not found.");
    }

}
